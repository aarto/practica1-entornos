package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		int opcion;
		
		System.out.println("1 - comprobar cadena");
		System.out.println("2 - cifras separadas");
		System.out.println("3 - salir");
		
		opcion = input.nextInt();
		
		switch (opcion) {
		
		case 1:
			System.out.println("Introduce una cadena");
			
			input.nextLine();
			
			String cadena = input.nextLine();
			
			if (cadena.length() >= 3) {
				System.out.println(cadena.substring(0, 2));
				System.out.println(cadena.substring(1, 3));
			}
			else {
				System.out.println("No se cumple");
			}
		
		break;
		
		case 2:
			System.out.println("Introduce un numero de 3 cifras comprendido entre 100 y 999");
			
			input.nextLine();
			
			String numero = input.nextLine();
			
			
			System.out.println(numero.substring(0, 1));
			System.out.println(numero.substring(1, 2));
			System.out.println(numero.substring(2, 3));
		
		break;
		
		case 3:
			System.out.println("salir");
			
		break;
		
		default:
			System.out.println("Opcion no contemplada");
		
		}
		input.close();
	}

}
